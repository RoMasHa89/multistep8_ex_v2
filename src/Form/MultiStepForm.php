<?php
/**
* @file
* Contains \Drupal\multistep8_ex_v2\Form\MultiStepForm.
*/

namespace Drupal\multistep8_ex_v2\Form;

use Drupal\Core\Form\FormStateInterface;

class MultiStepForm extends MultistepFormBase {

  /**
   * @var $initialized
   */
  protected $initialized = FALSE;

  /**
   * @var $form_elements
   */
  protected $form_elements = [];

  /**
   * This is the main form constructor method.
   *
   * Fill you form elements and options arrays here.
   *
   * [Example]:
   *  To create elements for Step 1:
   *    $this->form_elements['step_1']['ELEMENT1'] = array( ... );
   *    $this->form_elements['step_1']['ELEMENT2'] = array( ... );
   *  To create elements for Step 2:
   *    $this->form_elements['step_2']['ELEMENT1'] = array( ... );
   *    $this->form_elements['step_2']['ELEMENT2'] = array( ... );
   *
   *  etc...
   */
  public function init() {

    $model_ops = array(
      '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004',
      '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012',
      '2013', '2014', '2015');

    $body_style_ops = array('Coupe', 'Sedan', 'Convertible', 'Hatchback',
      'Station wagon', 'SUV', 'Minivan', 'Full-size van', 'Pick-up');

    $gas_mileage_ops = array('20 mpg or less', '21 mpg or more', '26 mpg or more',
      '31 mpg or more', '36 mpg or more', '41 mpg or more');


    $this->form_elements['step_1']['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t(''),
      '#options' => $model_ops,
    ];

    $this->form_elements['step_1']['body_style'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Body Style'),
      '#description' => $this->t(''),
      '#options' => $body_style_ops,
    ];

    $this->form_elements['step_1']['gas_mileage'] = [
      '#type' => 'radios',
      '#title' => $this->t('Gas Mileage'),
      '#description' => $this->t(''),
      '#options' => $gas_mileage_ops,
    ];

    $this->form_elements['step_2']['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
    );

    $this->form_elements['step_2']['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
    );


    $this->form_elements['step_3']['age'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your age'),
    );

    $this->form_elements['step_3']['location'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your location'),
    );

    $this->store->set('step', 1);
    $this->store->set('steps_count', count($this->form_elements));
    $this->initialized = TRUE;
  }

  /**
  * {@inheritdoc}.
  */
  public function getFormId() {
    return 'multistep_form';
  }

  /**
  * {@inheritdoc}.
  */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (empty($this->initialized)) {
      $this->init();
    }
    $step = $this->store->get('step');
    $steps_count = $this->store->get('steps_count');
    if ($step <= $steps_count) {
      $form = parent::buildForm($form, $form_state);
      foreach ($this->form_elements["step_$step"] as $name => $element) {
        $form[$name] = $element;
        $form['title'] = array(
          '#type' => 'markup',
          '#markup' => t('<h2>Step %step_val:</h2>', array('%step_val' => $step)),
          '#weight' => -1,
        );
      }
    }
    else {
      $form = $this->form_elements;
      foreach ($form as &$form_step) {
        foreach ($form_step as $key => &$element) {
          if (!empty($element['#type'])) {
            if ($element['#type'] == 'checkboxes') {
              $element['#default_value'] =  $this->store->get($key) ? (array) $this->store->get($key) : (array) '';
            }
            else {
              $element['#default_value'] =  $this->store->get($key) ? $this->store->get($key) : '';
            }

            $element['#disabled'] = TRUE;
          }
        }
      }
      $form['title'] = array(
        '#type' => 'markup',
        '#markup' => t('<h2>Your Form Results:</h2>'),
        '#weight' => -1,
      );
      // Manage Form Data.
      $this->manageFormData();
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function storeKey($data = NULL) {
    $form_data = $this->store->get('form_data');
    $step = $this->store->get('step');
    $filter_keys = array(
      'form_build_id',
      'form_token',
      'form_id',
      'op',
    );
    drupal_set_message(t('<b>Previous choices:</b><br>'));
    foreach($data as $key => $value) {
      if (!in_array($key, $filter_keys) && !empty($this->form_elements["step_$step"][$key])) {
        $this->store->set($key, $value);
        if (isset($this->form_elements["step_$step"][$key]['#options'])) {
          $form_data[$key]['raw'] = $value;
          if (is_array($value)) {
            $value = implode(', ',
              array_keys(array_intersect(array_flip(
                $this->form_elements["step_$step"][$key]['#options']), $value))
            );
          }
          else {
            $value = $this->form_elements["step_$step"][$key]['#options'][$value];
          }
        }
        $title = $this->form_elements["step_$step"][$key]['#title'];
        if ($title) {
          drupal_set_message($title . ": " . $value);
          $form_data[$key]['title'] = $title;
        }
        else {
          drupal_set_message(t('Option: %value', array('%value' => $value)));
        }
        $form_data[$key]['value'] = $value;
        $this->store->set('form_data', $form_data);
      }
    }
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = $this->store->get('step');
    // Store current form elements.
    $this->storeKey($form_state->getUserInput());
    // Increase step counter.
    if ($form_state->getValue('step') == $step) {
      $this->store->set('step', ++$step);
    }
    // Rebuild Form.
    $form_state->setRebuild();
  }

  /**
   * Data management from the multistep form.
   */
  protected function manageFormData() {
    // Logic for saving data goes here...
    $formdata = $this->store->get('form_data');

    // Clear Storage data.
    $this->deleteStore();
    drupal_set_message($this->t('The form has been saved.'));
  }
}